package main

import (
	"encoding/json"
  "fmt"
	"log"
	"os"
)

func main() {

	var data map[string]interface{}
	err := json.NewDecoder(os.Stdin).Decode(&data)
	if err != nil {
		log.Fatalln(err)
	}
  fmt.Println("valid")
}
