import b from './builders/index'
import {schemas} from './support/schemas'

describe('dast schema', () => {

  it('should accept api fuzzing as a scan type', () => {
    const report = b.dast.report({
      scan: b.dast.scan({type: 'api_fuzzing'})
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })

  it('should validate location', () => {
    const report = b.dast.report({
      vulnerabilities: [b.dast.vulnerability({
        location: {
          hostname: 'http://server.com',
          method: 'POST',
          param: 'username',
          path: '/WebGoat/images/favicon.ico'
        }
      })]
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })

  it('should validate location with just a hostname', () => {
    const report = b.dast.report({
      vulnerabilities: [b.dast.vulnerability({
        location: {
          hostname: 'http://server.com',
          method: '',
          param: '',
          path: ''
        }
      })]
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })

  it('vulnerabilities require a location', () => {
    const report = b.dast.report({
      vulnerabilities: [b.dast.vulnerability({location: undefined})]
    })

    expect(schemas.dast.validate(report).errors).toContain('vulnerabilities[0] requires property "location"')
  })

  it('validates scanned resources', () => {
    const report = b.dast.report({
      scan: b.dast.scan({
        scanned_resources: [{
          method: 'POST',
          type: 'url',
          url: 'http://mysite.com'
        }]
      })
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })

  it('scan requires scanned resources', () => {
    const report = b.dast.report({
      scan: b.dast.scan({scanned_resources: undefined})
    })

    expect(schemas.dast.validate(report).errors).toContain('scan requires property "scanned_resources"')
  })

  it('vulnerability request allows empty body', () => {
    const report = b.dast.report({
      vulnerabilities: [b.dast.vulnerability({evidence: {
        request: {
          headers: [
            {name: 'Accept', value: '*/*'},
            {name: 'Connection', value: 'keep-alive'},
            {name: 'Host', value: 'nginx'},
            {name: 'User-Agent', value: 'python-requests/2.20.1'},
            {name: 'x-initiated-by', value: 'GitLab DAST'}
          ],
          method: 'GET',
          url: 'http://nginx/',
          body: '',
        },
        response: {
          headers: [
            {name: 'Accept-Ranges', value: 'bytes'},
            {name: 'Connection', value: 'keep-alive'},
            {name: 'Content-Length', value: '345'},
            {name: 'Content-Type', value: 'text/html'},
            {name: 'Date', value: 'Fri, 04 Dec 2020 03:26:13 GMT'},
            {name: 'ETag', value: '\'5f446591-159\''},
            {name: 'Last-Modified', value: 'Tue, 25 Aug 2020 01:12:49 GMT'},
            {name: 'Server', value: 'nginx/1.17.6'}
          ],
          reason_phrase: 'OK',
          status_code: 200
        },
        summary: 'This issue still applies to error type pages as they are affected by injection issues.'
      }
      })]
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })

  it('vulnerability response allows empty body', () => {
    const report = b.dast.report({
      vulnerabilities: [b.dast.vulnerability({evidence: {
        request: {
          headers: [
            {name: 'Accept', value: '*/*'},
            {name: 'Connection', value: 'keep-alive'},
            {name: 'Host', value: 'nginx'},
            {name: 'User-Agent', value: 'python-requests/2.20.1'},
            {name: 'x-initiated-by', value: 'GitLab DAST'}
          ],
          method: 'GET',
          url: 'http://nginx/',
        },
        response: {
          headers: [
            {name: 'Accept-Ranges', value: 'bytes'},
            {name: 'Connection', value: 'keep-alive'},
            {name: 'Content-Length', value: '345'},
            {name: 'Content-Type', value: 'text/html'},
            {name: 'Date', value: 'Fri, 04 Dec 2020 03:26:13 GMT'},
            {name: 'ETag', value: '\'5f446591-159\''},
            {name: 'Last-Modified', value: 'Tue, 25 Aug 2020 01:12:49 GMT'},
            {name: 'Server', value: 'nginx/1.17.6'}
          ],
          reason_phrase: 'OK',
          status_code: 200,
          body: ''
        },
        summary: 'This issue still applies to error type pages as they are affected by injection issues.'
      }
      })]
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })

})
