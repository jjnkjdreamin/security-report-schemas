import {merge} from '../../support/merge'

const defaults = {
  'path': 'pom.xml',
  'package_manager': 'maven',
  'dependencies': [
    {
      'package': {'name': 'antlr/antlr'},
      'version': '2.7.7'
    },
    {
      'package': {'name': 'com.fasterxml.jackson.core/jackson-annotations'},
      'version': '2.9.0'
    }
  ]
}

export const dependency_files = (dependencyFileOverrides) => {
  return merge(defaults, dependencyFileOverrides)
}
