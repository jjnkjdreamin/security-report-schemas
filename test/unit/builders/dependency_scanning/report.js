import {merge} from '../../support/merge'
import {scan} from './scan'
import {vulnerability} from './vulnerability'
import {dependency_files} from './dependency_files'

const defaults = {
  remediations: [],
  scan: scan(),
  version: '100000.0.0',
  dependency_files: [dependency_files()],
  vulnerabilities: [vulnerability()]
}

export const report = (reportOverrides) => {
  return merge(defaults, reportOverrides)
}
