import {report} from './report'
import {vulnerability} from './vulnerability'
import {scan} from './scan'

export default {
  report,
  vulnerability,
  scan
}
