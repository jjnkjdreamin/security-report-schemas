import {merge} from '../../support/merge'

const defaults = {
  scanner: {
    id: 'libfuzzer',
    name: 'libfuzzer',
    url: 'https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing',
    vendor: {
      name: 'GitLab'
    },
    version: '4422b4d4'
  },
  type: 'coverage_fuzzing',
  start_time: '2020-12-10T17:40:07',
  end_time: '2020-12-10T17:40:17',
  status: 'success'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
